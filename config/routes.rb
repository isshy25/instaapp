Rails.application.routes.draw do
  root 'static_pages#home'
  get 'static_pages/home'
  get  '/terms',    to: 'static_pages#terms'
  
  devise_for :users

  resources :users, only: [:index, :show] do
    member do
      get :following, :followers
    end
  end
  resources :posts, only: [:create, :destroy, :show] do
    resources :likes, only: [:create, :destroy]
    resources :comments, only: [:create, :destroy]
  end
  resources :relationships, only: [:create, :destroy]
end
