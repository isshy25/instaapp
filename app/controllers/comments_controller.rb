class CommentsController < ApplicationController
  def create
   @post = Post.find_by(id: params[:post_id])
   @comment = current_user.comments.build(comment_params)
   @comment.save
   flash[:success] = "コメントに登録しました"
   redirect_to @post
  end


  def destroy
    @comment = Comment.find_by(id: params[:id])
    byebug
    @post = Post.find_by(id: @comment.post_id)
    @comment.destroy
    flash[:notice] = "コメントを削除しました"
  end

  private
  
  def comment_params
    params.require(:comment).permit(:user_id, :post_id, :content)
  end
end
