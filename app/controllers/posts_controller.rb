class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user,   only: :destroy
  
  def create
    @post = current_user.posts.build(post_params)
    
    if @post.save
      flash[:success] = "投稿に成功しました。!"
      redirect_to post_path(@post)
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "削除しました。"
    redirect_to  root_url
  end
  
  def show
    @post = Post.find_by(id: params[:id])
    @comment = Comment.new
    @comments = @post.comments
    @like = Like.new
  end

  private

    def post_params
      params.require(:post).permit(:picture)
    end

    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end

end
