class RemoveIndexEmailFromUsers < ActiveRecord::Migration[5.1]
  def change
     
    def up
       remove_index :users, :email
    end

    def down
       add_index :users, :email, unique: true
    end
    
  end
end
