class AddBasicinfoToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :name, :string
    add_column :users, :website, :string
    add_column :users, :information, :text
    add_column :users, :phone, :string
    add_column :users, :gender, :string
  end
end
